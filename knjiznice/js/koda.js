var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';
var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

var ehrIds = ["be2c01b8-2737-4858-9c41-c1d1b9a8fdaa","fbcff742-6fdc-4d74-8c5d-5539344edb8b","43d048ff-4ecd-4566-8d7f-dbcba5984d52"]

function restore() {
    generirajPodatke(); //dobiš ehrIds

    //pac 1
    dodajZdravilo("Lipitor","daily","pill",1, ehrIds[0])
    dodajZdravilo("pravastatin","per hour","capsules",3, ehrIds[0])
    dodajZdravilo("Cholestyramine Light ","every week","pill",1, ehrIds[0])
    dodajZdravilo("Lipofen","every 6h","capsules",2, ehrIds[0])
    dodajZdravilo("Fibricor","before sleep","spoon",1, ehrIds[0])
    dodajAlergijo(ehrIds[0], "Caduet - rash")
    dodajAlergijo(ehrIds[0], "paracetamol - liver damage")
    dodajAlergijo(ehrIds[0], "lactose - bloating")
    krvnitest(ehrIds[0],188, 2010, 01)
    krvnitest(ehrIds[0],180, 2010, 12)
    krvnitest(ehrIds[0],177, 2011, 04)
    krvnitest(ehrIds[0],168, 2011, 10)
    krvnitest(ehrIds[0],160, 2012, 04)
    krvnitest(ehrIds[0],155, 2013, 06)
    krvnitest(ehrIds[0],148, 2014, 01)
    krvnitest(ehrIds[0],145, 2015, 05)
    krvnitest(ehrIds[0],150, 2015, 10)
    krvnitest(ehrIds[0],142, 2016, 02)
    krvnitest(ehrIds[0],141, 2016, 04)
    krvnitest(ehrIds[0],139, 2017, 05)
    //pac 2
    dodajZdravilo("fluvastatin","daily","capsule",2, ehrIds[1])
    dodajZdravilo("Triglide","as needed","pill",1, ehrIds[1])
    dodajZdravilo("Caduet","monthly","injecton",1, ehrIds[1])
    dodajZdravilo("Vytorin","daily","pill",2, ehrIds[1])
    dodajAlergijo(ehrIds[1], "Fibricor - nausea")
    krvnitest(ehrIds[1],175, 2012, 01)
    krvnitest(ehrIds[1],170, 2013, 04)
    krvnitest(ehrIds[1],165, 2014, 01)
    krvnitest(ehrIds[1],162, 2014, 08)
    krvnitest(ehrIds[1],155, 2015, 04)
    krvnitest(ehrIds[1],151, 2015, 07)
    krvnitest(ehrIds[1],142, 2016, 03)
    krvnitest(ehrIds[1],138, 2016, 10)
    krvnitest(ehrIds[1],132, 2017, 02)

    //pac 3
    dodajZdravilo("Lipitor","monthly","pill",1, ehrIds[2])
    dodajZdravilo("evolocumab","as needed","pill",1, ehrIds[2])
    dodajAlergijo(ehrIds[2], "Triglide - hypertension")
    dodajAlergijo(ehrIds[2], "Triamterene - fever")
    dodajAlergijo(ehrIds[2], "loratidine - drowsiness")
    krvnitest(ehrIds[2],156, 2015, 06)
    krvnitest(ehrIds[2],152, 2016, 04)
    krvnitest(ehrIds[2],142, 2016, 10)
    krvnitest(ehrIds[2],130, 2017, 03)
}

function generirajPodatke2(ime, priimek, letR, mesR, danR, temp, vis,tez, imevnas) {
    var ehrId;
  //credentials
  var seja = getSessionId();
  $.ajaxSetup({
    headers: {
        "Ehr-Session": seja
    }, async: false
  });
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function (data) {
        ehrId = data.ehrId;//!!!!!!!!izpisi
        var partyData = {
            firstNames: ime,
            lastNames: priimek,
            dateOfBirth: letR + "-" + mesR+"-" + danR +"T19:30",
            partyAdditionalInfo: [{key: "ehrId",value: ehrId}]
        };
  
       $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            success: function (party) {
                if (party.action == 'CREATE') {
                    $("#new").append("<h4>" + ehrId + "</h4>")
                    console.log(ehrId);
                }
            }
        }); 
    }
    });
  console.log(temp+""+vis+""+tez)
    var compositionData = {
    "ctx/time": (new Date()).toString,
    "ctx/language": "en",
    "ctx/territory": "SI",
    "vital_signs/body_temperature/any_event/temperature|magnitude": temp,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/blood_pressure/any_event/systolic": 148,
    "vital_signs/blood_pressure/any_event/diastolic": 95,
    "vital_signs/height_length/any_event/body_height_length|magnitude": vis,
    "vital_signs/height_length:0/any_event:0/body_height_length|unit":"cm",
    "vital_signs/body_weight/any_event/body_weight": tez
    };
    var queryParams = {
        "ehrId": ehrId,
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: imevnas
    };
    $.ajax({
        url: baseUrl + "/composition?"+ $.param(queryParams),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(compositionData),
        success: function (res) {
            console.log("success")
        }
    });
}

//generira 3 predefinirane vzorčne paciente z imenom, rojstvom, tel.temp, tel.viš, tel.tež in krv. tlakom 
function generirajPodatke() {

  //credentials
  var seja = getSessionId();
  $.ajaxSetup({
    headers: {
        "Ehr-Session": seja
    }, async: false
  });
  //bolnik 1, star moški z zdravstvenimi težavami
  $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function (data) {
        var ehrId = data.ehrId;
        ehrIds[0] = ehrId;
        var partyData = {
            firstNames: "Anton",
            lastNames: "Podlesnik (VP1)",
            dateOfBirth: "1950-4-18T19:30",
            partyAdditionalInfo: [{key: "ehrId",value: ehrId}]
        };
  
       $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            success: function (party) {
                if (party.action == 'CREATE') {
                	console.log(ehrId);
                }
            }
        }); 
    }
    });
    var compositionData = {
    "ctx/time": "2014-3-19T13:10Z",
    "ctx/language": "en",
    "ctx/territory": "SI",
    "vital_signs/body_temperature/any_event/temperature|magnitude": 35.1,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/blood_pressure/any_event/systolic": 148,
    "vital_signs/blood_pressure/any_event/diastolic": 95,
    "vital_signs/height_length/any_event/body_height_length|magnitude": 178,
    "vital_signs/height_length:0/any_event:0/body_height_length|unit":"cm",
    "vital_signs/body_weight/any_event/body_weight": 90.2
    };
    var queryParams = {
        "ehrId": ehrIds[0],
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: 'Sestra 1'
    };
    $.ajax({
        url: baseUrl + "/composition?"+ $.param(queryParams),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(compositionData),
        success: function (res) {

        }
    });
    //bolnik 2-zdrava ženska srednjih let
    $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function (data) {
        var ehrId = data.ehrId;
        ehrIds[1] = ehrId;
        var partyData = {
            firstNames: "Polona",
            lastNames: "Volk (VP2)",
            dateOfBirth: "1985-2-28T10:55",
            partyAdditionalInfo: [{key: "ehrId",value: ehrId}]
        };
  
       $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            success: function (party) {
                if (party.action == 'CREATE') {
                    console.log(ehrId);
                }
            }
        }); 
    }
    });
    var compositionData = {
    "ctx/time": "2014-3-19T13:10Z",
    "ctx/language": "en",
    "ctx/territory": "SI",
    "vital_signs/body_temperature/any_event/temperature|magnitude": 37.1,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/blood_pressure/any_event/systolic": 115,
    "vital_signs/blood_pressure/any_event/diastolic": 75,
    "vital_signs/height_length/any_event/body_height_length": 170,
    "vital_signs/height_length:0/any_event:0/body_height_length|unit":"cm",
    "vital_signs/body_weight/any_event/body_weight": 60.3
    };
    var queryParams = {
        "ehrId": ehrIds[1],
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: 'Sestra 1'
    };
    $.ajax({
        url: baseUrl + "/composition?"+ $.param(queryParams),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(compositionData),
        success: function (res) {
            console.log(res)
        }
    });
    //bolnik 3 najstnik
      $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        success: function (data) {
            var ehrId = data.ehrId;
            ehrIds[2] = ehrId;
            var partyData = {
                firstNames: "Peter",
                lastNames: "Marolt (VP3)",
                dateOfBirth: "2000-9-1T01:04",
                partyAdditionalInfo: [{key: "ehrId",value: ehrId}]
            };
      
           $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action == 'CREATE') {
                    	console.log(ehrId);
                    }
                }
            }); 
        }
        });
    var compositionData = {
    "ctx/time": "2014-3-19T13:10Z",
    "ctx/language": "en",
    "ctx/territory": "SI",
    "vital_signs/body_temperature/any_event/temperature|magnitude": 36.7,
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/blood_pressure/any_event/systolic": 148,
    "vital_signs/blood_pressure/any_event/diastolic": 95,
    "vital_signs/height_length/any_event/body_height_length": 180,
    "vital_signs/height_length:0/any_event:0/body_height_length|unit":"cm",
    "vital_signs/body_weight/any_event/body_weight": 101.4
    };
    var queryParams = {
        "ehrId": ehrIds[2],
        templateId: 'Vital Signs',
        format: 'FLAT',
        committer: 'Sestra 1'
    };
    $.ajax({
        url: baseUrl + "/composition?"+ $.param(queryParams),
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(compositionData),
        success: function (res) {
            console.log(res)
        }
    });

    try{
        for (var i = 0; i < 3; i++) {
            $("#genap").append("<h4>" + ehrIds[i] + "</h4>")
        }
        
    }
    catch(err){
        console.log("napaka pri generiranju pacientov");
    }
} 
//doda zdravilo, interval jemanja,kaj se jemlje, koliko kosov, st.pacienta
function dodajZdravilo(zdravilo, cas, oblika, stevilo, ehrId) {
    var seja = getSessionId();
    $.ajaxSetup({
    headers: {
        "Ehr-Session": seja
    }, async: false
    });
    
    var compositionData = {
    "ctx/language":"en",
    "ctx/territory":"SI",
    "ctx/time":(new Date()).toString,
        
    "ctx/participation_name":"Dr. Novak",//kdo predpisal
    "ctx/participation_function":"requester",
        
    "medications/medication_instruction:0/order:0/medicine":zdravilo,//ime zdravila
    "medications/medication_instruction:0/order:0/directions":oblika,
    "medications/medication_instruction:0/order:0/dose/quantity|magnitude":54.29, 
    "medications/medication_instruction:0/order:0/dose/quantity|unit":"?",
    "medications/medication_instruction:0/order:0/dose/description":oblika,
    "medications/medication_instruction:0/order:0/medication_timing/timing_description":cas,
    "medications/medication_instruction:0/order:0/medication_timing/timing/daily_count":stevilo,
    "medications/medication_instruction:0/order:0/medication_timing/start_date":(new Date()).toString,
    "medications/medication_instruction:0/order:0/medication_timing/stop_date":"2018-05-27T13:45:46.313Z",
    "medications/medication_instruction:0/order:0/medication_timing/long-term":false,
    "medications/medication_instruction/order/timing|formalism": "timing",
    "medications/medication_instruction/order/timing|value": 10,
    "medications/medication_instruction/narrative": "none"
    }
    
    var queryParams = {
    "ehrId": ehrId,
    templateId: 'Medications',
    format: 'FLAT',
    committer: 'Sestra 1'
    };
    
    $.ajax({
    async:false,
    url: baseUrl + "/composition?" + $.param(queryParams),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(compositionData),
    success: function (res) {
        console.log("Novo zdravilo dodano " + new Date().toDateString());
        console.log(res)
    }
    });   
}

//doda občutljivost na zdravilno učinkovino
function dodajAlergijo(ehr, substanca) {
    var seja = getSessionId();
    $.ajaxSetup({
    headers: {
        "Ehr-Session": seja
    }, async: false
    });
    var compositionData = {
       "ctx/language":"en",
       "ctx/territory":"SI",
       "ctx/time":(new Date().toString),
       "ctx/participation_name":"Dr. Novak",
       "ctx/participation_function":"requester",

       "allergies/adverse_reaction_-_allergy:0/substance_agent":substanca,
       "allergies/adverse_reaction_-_allergy:0/recorded":(new Date().toString),
       "allergies/adverse_reaction_-_allergy:0/reaction_reported":true
    }
    
    var queryParams = {
    "ehrId": ehr,
    templateId: 'Allergies',
    format: 'FLAT',
    committer: 'Sestra 1'
    };
    $.ajax({
    async:false,
    url: baseUrl + "/composition?" + $.param(queryParams),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(compositionData),
    success: function (res) {
        console.log("Nova alergija dodana " + new Date().toDateString());
        console.log(res)
    }
    });
}

//pacientu doda nivo(int) ldl holesterola na dane leto in mesec
// ok: pod 100, tvegan: do 130, slab nad 160, zelo slab od 190
function krvnitest(ehr, nivo, leto, mesec) {
    leto = leto.toString();
    mesec = mesec.toString();
    var seja = getSessionId();
    $.ajaxSetup({
    headers: {
        "Ehr-Session": seja
    }, async: false
    });
    
     var compositionData ={
        "ctx/language":"en",
        "ctx/territory":"SI",
        "ctx/time":"2017-05-27T21:47:12.511+02:00",
        "ctx/participation_name":"Dr. Novak",
        "ctx/participation_function":"requester",
         "laboratory_report/laboratory_test_result:0/any_event:0/specimen:0/specimen_tissue_type":"Blood",
         "laboratory_report/laboratory_test_result:0/any_event:0/specimen:0/collection_details/datetime_collection_start":"2017-05-28T11:56:42.359Z",
        "laboratory_report/laboratory_test_result:0/any_event:0/test_result_name":"Test result name 82", 
        "laboratory_report/laboratory_test_result:0/any_event:0/specimen:0/collection_details/datetime_collection_start":"2017-05-27T22:14:14.772Z",
         "laboratory_report/laboratory_test_result:0/any_event:0/overall_test_result_status|code":"at0074",
         "laboratory_report/laboratory_test_result:0/any_event:0/result_group:0/result:0/result_value/value|magnitude":12.69,"laboratory_report/laboratory_test_result:0/any_event:0/result_group:0/result:0/result_value/value|unit":"mg/dl",  
         "laboratory_report/laboratory_test_result:0/any_event:0/result_group:0/result:0/result_value/value2":nivo,
         "laboratory_report/laboratory_test_result:0/any_event:0/result_group:0/result:0/result_comment:0":"Result comment 62",
         "laboratory_report/laboratory_test_result:0/any_event:0/result_group:0/result:0/date_time_result_issued":leto +"-" + mesec+"-11T10:05"
    }
    var queryParams = {
    "ehrId": ehr,
    templateId: 'Laboratory Report',
    format: 'FLAT',
    committer: 'Sestra 1'
    };
    $.ajax({
    
    url: baseUrl + "/composition?" + $.param(queryParams),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(compositionData),
    success: function (res) {
        console.log("Nova krvna slika dodana " + new Date().toDateString());
        console.log(res)
    }
    });      
}

function prikaz(ehrId) {
    $("#zdravila").empty(); 
    $("#zdravila").append("<h5>Current medications</h5>");  
    $("#meritve").empty();
    $("#meritve").append("<h5>Body measurements</h5>");
    $("#oseba").empty();
    $("#oseba").append("<h5>Patient data</h5>");
    $("#lab").empty();
    $("#lab").append("<h5>Cholesterol(LDL) levels</h5>");
    $("#alergije").empty();
    $("#alergije").append("<h5>Drug intolerances</h5>");
    

    var seja = getSessionId();
    $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    headers: {
        "Ehr-Session": seja
    },async:false,
    success: function (data) {
        //console.log(data);
        var oseba = data.party;
        /*document.getElementById().HTML ="<p>" + oseba.firstNames + " " + oseba.lastNames + "</p>" + "<p>"+ " Born: " + oseba.dateOfBirth +"<p>";*/
        $("#oseba").append("<p>" + oseba.firstNames + " " + oseba.lastNames + "</p>"+ "<p>"+ " Born: " + oseba.dateOfBirth.split('T')[0] +"<p>");
    }
    });

    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    headers: {
        "Ehr-Session": seja
    },
    success: function (res) {
        //console.log(res);
        for (var i in res) {
            $("#meritve").append(res[i].time.split('T')[0] + ': ' + res[i].weight + res[i].unit + "<br>");
        }
    }
        
    });
    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/height",
    type: 'GET',
    headers: {
        "Ehr-Session": seja
    },
    success: function (res) {
        for (var i in res) {
            $("#meritve").append(res[i].time.split('T')[0]  + ': ' + res[i].height + res[i].unit + "<br>");
        }
    }
    });
    
    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/medication",
    type: 'GET',
    headers: {
        "Ehr-Session": seja
    },
    success: function (res) {
        //console.log("zdravila:")
       // console.log(res)
        for (var i in res) {
            $("#zdravila").append(res[i].medicine + ': ' + res[i].daily_count +" "+res[i].description +" "+ res[i].timing + "<br>");
        }
    }
    });

    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/labs",
    type: 'GET',
    headers: {
        "Ehr-Session": seja
    },
    success: function (res) {
        //console.log(res)
        var data = new Array();
        for (var i in res) {
            data[i] = res[i].value;
            $("#lab").append(res[i].time.split('T')[0] + ': ' + res[i].name +" "+ res[i].value + " " + res[i].unit+  "<br>");
        }

        $(".chart").empty();
        for (var i = data.length - 1; i >= 0; i--) {
            $(".chart").append("<div onmouseover=\"overlay(this)\" class = 'coll' id = 'cl" + i  + "' style='width:"+data[i]*3+ "px;'>"+data[i] + " mg/dl" +"</div>")
        }
    }
    });

    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/allergy",
    type: 'GET',
    headers: {
        "Ehr-Session": seja
    },
    success: function (res) {
        //console.log(res)
        for (var i in res) {
            $("#alergije").append(res[i].agent  + "<br>");
        }
    }
    });  
}

function retData() {
    return data
}
function overlay(e) {

    var x = "<h3>LDL by time(earliest to latest)</h3>";
    $("#viz2").empty();
    if(e.innerHTML.split(' ')[0] > 190){ $("#viz2").append(x + "<div>VERY HIGH</div>"); $("#viz2").css("background-color", "red");}
    else if(e.innerHTML.split(' ')[0] > 160){ $("#viz2").append(x + "<div>HIGH</div>"); $("#viz2").css("background-color", "red");}
    else if(e.innerHTML.split(' ')[0] > 130){ $("#viz2").append(x + "<div>AT RISK</div>"); $("#viz2").css("background-color", "yellow");}
    else if(e.innerHTML.split(' ')[0] > 100){ $("#viz2").append(x + "<div>HEALTHY</div>"); $("#viz2").css("background-color", "green");}
    else{ $("#viz2").val(x + "<div>HEALTHY</div>"); $("#viz2").css("background-color", "green");}
}
window.onload = function() {
    //prikaz(ehrIds[0]);
    $.ajax({
        async:false,
        url: 'https://ipinfo.io/json',
        type: 'GET',
        success: function(res) {
        console.log(res.city);
        $("#map").append(" (" + res.city+ ")");
        try {
        document.getElementById("map").innerHTML = "<p id=\"map\"><h5><b>Pharmacies nearby(" + res.city+
        "):</b></h5> "+ "<iframe width=\"500\" height=\"500\" frameborder=\"0\"\ style=\"border:0\" src=\"https://www.google.com/maps/embed/v1/search?q=Lekarna%20"
        + res.city +"&key=AIzaSyB0FW-4IkM9b7aoJE2poCl9J8azWOi6xic\" allowfullscreen></iframe>"}
        catch(err) {
            console.log()
        }
    }
    });
    
    
}



